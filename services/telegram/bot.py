import os
import requests
import logging


def send_message(user_id, message):
    token = os.environ.get('TELEGRAM_BOT_TOKEN')
    url = 'https://api.telegram.org/bot' + token + '/sendMessage?chat_id=' + user_id + '&text=' + message
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()['result']
        bot_name = data['from']['username']
        user_name = data['chat']['username']
        message = data['text']
        logging.info(f'Telegram BOT @{bot_name} sending message to user @{user_name}: {message}')
    else:
        logging.error('Telegram BOT Error')
