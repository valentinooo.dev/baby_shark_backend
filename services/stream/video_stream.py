import cv2
import imagezmq
import os

image_hub = imagezmq.ImageHub(open_port=f'tcp://{os.environ.get("STREAM_IP")}:5555', REQ_REP=False)
image_hub.connect(f'tcp://{os.environ.get("STREAM_IP")}:5555')


def stream():
    try:
        while True:
            msg, img = image_hub.recv_image()
            if not msg:
                print("Error: failed to capture image")
                break
            # img = cv2.resize(img, (320, 240))
            status = cv2.imwrite('demo.jpg', img)
            if not status:
                print('write fail')
            else:
                # print(msg)
                pass
                # image_hub.send_reply(b'OK')
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + open('demo.jpg', 'rb').read() + b'\r\n')
    except Exception as e:
        print(e)
        pass
