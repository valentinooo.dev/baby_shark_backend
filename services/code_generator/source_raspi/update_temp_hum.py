import Adafruit_DHT
import requests
import time

DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN = 4
DEVICE_ID = $DEVICE_ID
BACKEND_IP = '$SERVER_IP'
URL = f'http://{SERVER_IP}:8000/api/update-temp-hum/{DEVICE_ID}'

while True:
    humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)
    if humidity is not None and temperature is not None:
        data = {
            'temperature': f'{temperature:.2f}',
            'humidity': f'{humidity:.2f}'
        }
        requests.post(URL, json=data)
        print(data)
    else:
        print("Failed to retrieve data from humidity sensor")
    time.sleep(1)
