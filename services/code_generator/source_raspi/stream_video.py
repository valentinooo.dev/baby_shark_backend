import cv2
import imagezmq
import time

# Accept connections on all tcp addresses, port 5555
sender = imagezmq.ImageSender(connect_to='tcp://*:5555', REQ_REP=False)

rpi_name = 'a'
cam = cv2.VideoCapture(-1, cv2.CAP_V4L)
time.sleep(2.0)  # allow camera sensor to warm up
while True:  # send images until Ctrl-C
    status, image = cam.read()
    if status:
        resize = cv2.resize(image, (320, 240))
        sender.send_image(rpi_name, resize)
        # print('sent')
    else:
        cam.release()
        # print('fail')
        break
