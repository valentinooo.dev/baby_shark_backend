import os
from zipfile import ZipFile, ZIP_DEFLATED

from django.conf import settings


def gen_code_nodemcu(device_id, server_ip):
    source_path = os.path.join(settings.BASE_DIR, 'services/code_generator/source_nodemcu/source.ino')
    new_code_path = os.path.join(settings.BASE_DIR, f'services/code_generator/generated/code-{device_id}.ino')
    with open(source_path) as source_code:
        code = source_code.read()
        code = code.replace('DEVICE_ID', device_id)
        code = code.replace('SERVER_IP', server_ip)
    with open(new_code_path, 'w') as new_code:
        new_code.write(code)
    return True


def gen_code_raspi(device_id, server_ip):
    stream_video_path = os.path.join(settings.BASE_DIR, f'services/code_generator/source_raspi/stream_video.py')
    cry_detect_path = os.path.join(settings.BASE_DIR, f'services/code_generator/source_raspi/cry_detect.py')
    new_cry_detect_path = os.path.join(settings.BASE_DIR,
                                       f'services/code_generator/generated/cry_detect-{device_id}.py')
    update_temp_hum_path = os.path.join(settings.BASE_DIR, f'services/code_generator/source_raspi/update_temp_hum.py')
    new_update_temp_hum_path = os.path.join(settings.BASE_DIR,
                                            f'services/code_generator/generated/update_temp_hum-{device_id}.py')
    new_code_path = os.path.join(settings.BASE_DIR, f'services/code_generator/generated/code-{device_id}.zip')

    with open(cry_detect_path) as cry_detect_code:
        code_cry_detect = cry_detect_code.read()
        code_cry_detect = code_cry_detect.replace('$DEVICE_ID', device_id)
        code_cry_detect = code_cry_detect.replace('$SERVER_IP', server_ip)

    with open(new_cry_detect_path, 'w') as new_code:
        new_code.write(code_cry_detect)

    with open(update_temp_hum_path) as update_temp_hum_code:
        code_update_temp_hum = update_temp_hum_code.read()
        code_update_temp_hum = code_update_temp_hum.replace('$DEVICE_ID', device_id)
        code_update_temp_hum = code_update_temp_hum.replace('$SERVER_IP', server_ip)

    with open(new_update_temp_hum_path, 'w') as new_code:
        new_code.write(code_update_temp_hum)

    with ZipFile(new_code_path, 'w') as zf:
    # Add multiple files to the zip
        zf.write(new_cry_detect_path)
        zf.write(new_update_temp_hum_path)
        zf.write(new_update_temp_hum_path)
    return True
