import os
import pickle

from django.conf import settings

from services.cry_detector.rpi_methods import Reader
from services.cry_detector.rpi_methods.feature_engineer import FeatureEngineer
from services.cry_detector.rpi_methods.majority_voter import MajorityVoter
from services.cry_detector.rpi_methods.baby_cry_predictor import BabyCryPredictor


def predict(file_path=None):
    if file_path is not None:
        audio_path = os.path.join(settings.BASE_DIR, file_path)
    else:
        audio_path = os.path.join(settings.BASE_DIR, 'services/cry_detector/audio_file.mp3')
    file_reader = Reader(audio_path)
    play_list = file_reader.read_audio_file()

    engineer = FeatureEngineer()
    play_list_processed = list()

    for signal in play_list:
        tmp = engineer.feature_engineer(signal)
        play_list_processed.append(tmp)

    model_path = os.path.join(settings.BASE_DIR, 'services/cry_detector/models/model.pkl')
    with open(model_path, 'rb') as fp:
        model = pickle.load(fp)
    predictor = BabyCryPredictor(model)

    predictions = list()

    for signal in play_list_processed:
        tmp = predictor.classify(signal)
        predictions.append(tmp)
    majority_voter = MajorityVoter(predictions)
    majority_vote = majority_voter.vote()
    return majority_vote
