# syntax=docker/dockerfile:1
FROM python:3.8.13-slim-buster
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
RUN apt update && apt install -y gcc
RUN apt install -y default-libmysqlclient-dev
RUN apt install -y ffmpeg libsm6 libxext6
COPY requirements.txt /code/
RUN pip install --upgrade pip && pip install -r requirements.txt
COPY . /code/