from django.db import models
from datetime import datetime
from django.contrib.auth.models import User


class CustomModelManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(deleted_at=None)


class BaseModel(models.Model):
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = CustomModelManager()
    all_objects = models.Manager()

    def soft_delete(self):
        self.deleted_at = datetime.now()
        self.save()

    def restore(self):
        self.deleted_at = None
        self.save()

    class Meta:
        abstract = True


class SystemModel(BaseModel):
    name = models.CharField(max_length=255)
    owner = models.OneToOneField(User, related_name='system', on_delete=models.CASCADE)
    telegram_id = models.CharField(max_length=255)
    highest_temperature = models.DecimalField(max_digits=10, decimal_places=2, default=30)
    lowest_temperature = models.DecimalField(max_digits=10, decimal_places=2, default=20)
    highest_humidity = models.DecimalField(max_digits=10, decimal_places=2, default=80)
    lowest_humidity = models.DecimalField(max_digits=10, decimal_places=2, default=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Systems'
        db_table = 'systems'


class NodeMCUDeviceModel(BaseModel):
    name = models.CharField(max_length=255)
    system = models.ForeignKey(SystemModel, on_delete=models.CASCADE, related_name='nodemcus')

    def __str__(self):
        return f'({self.id}) {self.name}'

    class Meta:
        verbose_name_plural = 'NodeMCU Devices'
        db_table = 'nodemcus'
        ordering = ('-created_at',)


class NodeMCUHistoryModel(BaseModel):
    device = models.ForeignKey(NodeMCUDeviceModel, on_delete=models.CASCADE, related_name='histories')
    status = models.BooleanField()

    def __str__(self):
        return f'{self.device.name} - {self.status} - {self.updated_at}'

    class Meta:
        verbose_name_plural = 'Node MCU Histories'
        db_table = 'nodemcu_histories'
        ordering = ('-created_at',)


class RaspiDeviceModel(BaseModel):
    name = models.CharField(max_length=255)
    system = models.ForeignKey(SystemModel, on_delete=models.CASCADE, related_name='raspis')

    def __str__(self):
        return f'({self.id}) {self.name}'

    class Meta:
        verbose_name_plural = 'Raspi Devices'
        db_table = 'raspis'
        ordering = ('-created_at',)


class RaspiHistoryModel(BaseModel):
    device = models.ForeignKey(RaspiDeviceModel, on_delete=models.CASCADE, related_name='histories')
    cry_status = models.BooleanField()
    temperature = models.DecimalField(max_digits=10, decimal_places=2)
    humidity = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f'{self.device.name} - {self.cry_status} - {self.updated_at}'

    class Meta:
        verbose_name_plural = 'Raspi Histories'
        db_table = 'raspi_histories'
        ordering = ('-created_at',)
