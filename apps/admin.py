from django.contrib import admin
from .models import (
    SystemModel,
    NodeMCUDeviceModel,
    NodeMCUHistoryModel,
    RaspiDeviceModel,
    RaspiHistoryModel
)


admin.site.register(SystemModel)
admin.site.register(NodeMCUDeviceModel)
admin.site.register(NodeMCUHistoryModel)
admin.site.register(RaspiDeviceModel)
admin.site.register(RaspiHistoryModel)
