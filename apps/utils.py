import datetime


def format_time(time):
    return time.strftime('%Y/%m/%d - %H:%M')
