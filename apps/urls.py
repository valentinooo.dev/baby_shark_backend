from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from apps import views

app_name = 'apps'

urlpatterns = [
    # Auth
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', views.verify, name='token_verify'),

    path('test', views.test, name='test'),
    path('overview', views.overview, name='overview'),
    path('devices', views.device_list, name='device_list'),
    path('raspi/<int:pk>', views.raspi_detail, name='raspi_detail'),
    path('nodemcu/<int:pk>', views.nodemcu_detail, name='nodemcu_detail'),
    path('graph', views.graph, name='graph'),
    path('history', views.history, name='history'),
    path('video-feed', views.video_feed, name='video_feed'),
    path('test-video', views.test_video, name='test_video'),
    path('update-temp-hum/<int:pk>', views.temp_hum_update, name='update_temp_hum'),
    path('cry-detection/<int:pk>', views.cry_detection, name='cry_detection'),
    path('danger-zone-detection/<int:pk>', views.danger_zone_detected, name='cry_detection'),
    path('download-nodemcu-code/<int:pk>', views.download_nodemcu_code, name='download_nodemcu_code'),
    path('download-raspi-code/<int:pk>', views.download_raspi_code, name='download_raspi_code'),
    path('support-api/<int:pk>', views.support, name='support_api'),
    path('support', views.support_page, name='support_page'),
]
