import mimetypes
import os

from django.conf import settings
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework import status
from django.http import StreamingHttpResponse, HttpResponse
from django.template import loader
from django.db.models import Q

from datetime import datetime, timedelta

from .models import (
    SystemModel,
    NodeMCUDeviceModel,
    NodeMCUHistoryModel,
    RaspiDeviceModel,
    RaspiHistoryModel
)

from .serializers import (
    SystemInfoSerializer,
    NodeMCUDeviceSerializer,
    NodeMCUHistorySerializer,
    RaspiDeviceSerializer,
    RaspiHistorySerializer,
    SystemDetailSerializer,
    NodeMCUDetailSerializer,
    RaspiDetailSerializer,
    GraphSerializer,
    UpdateRaspiHistorySerializer,
    UpdateNodeMCUHistorySerializer,
)

from services.telegram import bot
from services.stream.video_stream import stream
from services.cry_detector.detector import predict
from services.code_generator import gen_code_nodemcu, gen_code_raspi


# User views
@api_view(['GET', 'POST'])
@authentication_classes([])
@permission_classes([])
def test(request):
    print(os.environ.get("TEST"))
    return Response({})


@api_view(['GET'])
def verify(request):
    user = request.user
    return Response({
        'username': user.username
    })


@api_view(['GET', 'PUT', 'DELETE'])
def overview(request):
    user = request.user
    system = user.system
    if system is None:
        return Response({
            'message': "You don't have any systems"
        }, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SystemInfoSerializer(system)
        return Response(serializer.data)

    elif request.method == 'PUT':
        data = request.data
        serializer = SystemDetailSerializer(system, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            response = serializer.data
            response['message'] = 'Updated Successfully'
            return Response(response)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        user = request.user
        systems = SystemModel.objects.filter(owner=user)
        count = systems.delete()
        return Response(f'{count[0]} systems were deleted successfully!')


@api_view(['GET', 'POST'])
def device_list(request):
    if request.method == 'GET':
        system = request.user.system
        raspis = system.raspis.all()
        nodemcus = system.nodemcus.all()

        raspi_serializer = RaspiDeviceSerializer(raspis, many=True)
        nodemcu_serializer = NodeMCUDeviceSerializer(nodemcus, many=True)
        return Response({
            'raspis': raspi_serializer.data,
            'nodemcus': nodemcu_serializer.data
        })

    elif request.method == 'POST':
        system = request.user.system
        data = {
            'system': system.id,
            'name': request.data.get('name')
        }
        if request.data.get('device_type') == 'raspi':
            serializer = RaspiDetailSerializer(data=data)
        elif request.data.get('device_type') == 'nodemcu':
            serializer = NodeMCUDetailSerializer(data=data)
        else:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE'])
def raspi_detail(request, pk):
    raspi = RaspiDeviceModel.objects.filter(id=pk).first()
    if raspi is None:
        return Response({'message': '404 Not Found'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        raspi.delete()
        return Response({'message': 'Deleted'}, status=status.HTTP_204_NO_CONTENT)


@api_view(['DELETE'])
def nodemcu_detail(request, pk):
    nodemcu = NodeMCUDeviceModel.objects.filter(id=pk).first()
    if nodemcu is None:
        return Response({'Message': '404 Not Found'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        nodemcu.delete()
        return Response({'Message': 'Deleted'}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def graph(request):
    system = request.user.system
    raspi = system.raspis.first()
    if raspi is None:
        return Response([])
    now = datetime.now()
    times = list()
    histories = list()
    for i in range(24):
        times.append(now - timedelta(hours=i))
    times.reverse()

    for time in times:
        history = RaspiHistoryModel.objects.filter(updated_at__day=time.day, updated_at__hour=time.hour).first()
        if history is not None:
            histories.append(history)
    histories_serializer = GraphSerializer(histories, many=True)
    output = list()
    for data in histories_serializer.data:
        output.append({
            'name': 'humidity',
            'value': data.get('humidity'),
            'time': data.get('time')
        })
        output.append({
            'name': 'temperature',
            'value': data.get('temperature'),
            'time': data.get('time')
        })
    return Response(output)


@api_view(['GET'])
def history(request):
    system = request.user.system
    raspis = system.raspis.all()
    nodemcus = system.nodemcus.all()
    raspi_histories = list()
    nodemcu_histories = list()
    for raspi in raspis:
        query_set = RaspiHistoryModel.objects.filter(device_id=raspi.id).filter(
            Q(cry_status=True) |
            Q(temperature__gt=system.highest_temperature) |
            Q(temperature__lt=system.lowest_temperature) |
            Q(humidity__gt=system.highest_humidity) |
            Q(humidity__lt=system.lowest_humidity)
        )
        for obj in query_set:
            raspi_histories.append(obj)
    for nodemcu in nodemcus:
        query_set = NodeMCUHistoryModel.objects.filter(device_id=nodemcu.id, status=True)
        for obj in query_set:
            nodemcu_histories.append(obj)
    raspi_serializer = RaspiHistorySerializer(raspi_histories, many=True)
    nodemcu_serializer = NodeMCUHistorySerializer(nodemcu_histories, many=True)
    histories = raspi_serializer.data + nodemcu_serializer.data
    response = sorted(histories, key=lambda x: x.get('time'), reverse=True)
    return Response(response)


def video_feed(request):
    return StreamingHttpResponse(stream(), content_type='multipart/x-mixed-replace; boundary=frame')


def test_video(request):
    template = loader.get_template('test_video.html')
    return HttpResponse(template.render({}, request))


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def temp_hum_update(request, pk):
    raspi = RaspiDeviceModel.objects.filter(id=pk).first()
    if raspi is None:
        return Response({'message': 'Raspi device does not exist'})
    data = request.data
    data['device'] = pk
    data['temperature'] = float(data["temperature"])
    data['humidity'] = float(data["humidity"])
    last_history = RaspiHistoryModel.objects.filter(device_id=pk).first()
    if last_history is None:
        data['cry_status'] = False
        serializer = UpdateRaspiHistorySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
    else:
        data['cry_status'] = last_history.cry_status
        if data['temperature'] != str(last_history.temperature) or data['humidity'] != str(last_history.humidity):
            if data['temperature'] > raspi.system.highest_temperature:
                bot.send_message(raspi.system.telegram_id,
                                 f'High temperature detected at {raspi.name}: {data["temperature"]}°C')
            elif data['temperature'] < raspi.system.lowest_temperature:
                bot.send_message(raspi.system.telegram_id,
                                 f'Low temperature detected at {raspi.name}: {data["temperature"]}°C')
            if data['humidity'] > raspi.system.highest_humidity:
                bot.send_message(raspi.system.telegram_id,
                                 f'High humidity detected at {raspi.name}: {data["humidity"]}%')
            elif data['humidity'] < raspi.system.lowest_humidity:
                bot.send_message(raspi.system.telegram_id,
                                 f'Low humidity detected at {raspi.name}: {data["humidity"]}%')

            now = datetime.now()
            day, hour = now.day, now.hour
            if day == last_history.created_at.day and hour == last_history.created_at.hour:
                raspi_history_serializer = UpdateRaspiHistorySerializer(last_history, data=data)
            else:
                raspi_history_serializer = UpdateRaspiHistorySerializer(data=data)
            if raspi_history_serializer.is_valid():
                raspi_history_serializer.save()
                return Response(raspi_history_serializer.data)
            return Response(raspi_history_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        else:
            raspi_history_serializer = UpdateRaspiHistorySerializer(last_history)
            return Response(raspi_history_serializer.data)


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def cry_detection(request, pk):
    raspi = RaspiDeviceModel.objects.filter(id=pk).first()
    if raspi is None:
        return Response({'message': 'Raspi device does not exist'})
    audio_file = request.FILES.get('audio_file')
    path = default_storage.save('services/cry_detector/audio_file.mp3', ContentFile(audio_file.read()))
    is_baby_cry = predict(path)

    if is_baby_cry:
        last_history = raspi.histories.first()
        data = {
            'device': pk,
            'cry_status': True
        }
        if last_history is not None:
            data['temperature'] = last_history.temperature
            data['humidity'] = last_history.humidity
        serializer = UpdateRaspiHistorySerializer(data=data)
        bot.send_message(raspi.system.telegram_id, f'Baby cry detected at {raspi.name}')
        if serializer.is_valid():
            serializer.save()
        else:
            print(serializer.errors)
    print('Baby cry:', is_baby_cry)
    return Response({
        'message': is_baby_cry
    })


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def danger_zone_detected(request, pk):
    nodemcu = NodeMCUDeviceModel.objects.filter(id=pk).first()
    if nodemcu is None:
        return Response({'message': 'Device does not exist'}, status=status.HTTP_404_NOT_FOUND)
    data = {
        'device': pk,
        'status': True
    }
    nodemcu_history_serializer = UpdateNodeMCUHistorySerializer(data=data)
    if nodemcu_history_serializer.is_valid():
        nodemcu_history_serializer.save()
        bot.send_message(nodemcu.system.telegram_id, f'Baby go to danger zone detected at {nodemcu.name}')
        return Response(nodemcu_history_serializer.data)
    return Response(nodemcu_history_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def download_nodemcu_code(request, pk):
    gen_code_nodemcu(str(pk), os.environ.get('SERVER_IP'))
    new_code_path = os.path.join(settings.BASE_DIR, f'services/code_generator/generated/code-{pk}.ino')
    code = open(new_code_path, 'r')
    mime_type, _ = mimetypes.guess_type(new_code_path)
    response = HttpResponse(code, content_type=mime_type)
    response['Content-Disposition'] = f'attachment; filename=code-{pk}.ino'
    return response


def download_raspi_code(request, pk):
    gen_code_raspi(str(pk), os.environ.get('SERVER_IP'))
    new_code_path = os.path.join(settings.BASE_DIR, f'services/code_generator/generated/code-{pk}.zip')
    code = open(new_code_path, 'r')
    mime_type, _ = mimetypes.guess_type(new_code_path)
    response = HttpResponse(code, content_type=mime_type)
    response['Content-Disposition'] = f'attachment; filename=code-{pk}.zip'
    return response


@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def support(request, pk):
    raspi = RaspiDeviceModel.objects.filter(id=pk).first()
    last_history = raspi.histories.first()
    data = {
        'device': pk,
        'cry_status': True
    }
    if last_history is not None:
        data['temperature'] = last_history.temperature
        data['humidity'] = last_history.humidity
    else:
        data['temperature'] = 0
        data['humidity'] = 0
    serializer = UpdateRaspiHistorySerializer(data=data)
    bot.send_message(raspi.system.telegram_id, f'Baby cry detected at {raspi.name}')
    if serializer.is_valid():
        serializer.save()
    else:
        print(serializer.errors)

    return Response({
        'message': 'OK'
    })


def support_page(request):
    template = loader.get_template('support.html')
    return HttpResponse(template.render({}, request))
