from rest_framework import serializers
from .models import (
    SystemModel,
    NodeMCUDeviceModel,
    NodeMCUHistoryModel,
    RaspiDeviceModel,
    RaspiHistoryModel
)

from .utils import format_time


class SystemInfoSerializer(serializers.ModelSerializer):
    raspi_counter = serializers.SerializerMethodField()
    nodemcu_counter = serializers.SerializerMethodField()
    temperature = serializers.SerializerMethodField()
    humidity = serializers.SerializerMethodField()
    cry_status = serializers.SerializerMethodField()

    def get_raspi_counter(self, obj):
        return obj.raspis.count()

    def get_nodemcu_counter(self, obj):
        return obj.nodemcus.count()

    def get_temperature(self, obj):
        raspi = obj.raspis.first()
        if raspi is not None:
            last_history = raspi.histories.first()
            return last_history.temperature
        return None

    def get_humidity(self, obj):
        raspi = obj.raspis.first()
        if raspi is not None:
            last_history = raspi.histories.first()
            return last_history.humidity

    def get_cry_status(self, obj):
        raspi = obj.raspis.first()
        if raspi is not None:
            last_history = raspi.histories.first()
            return last_history.cry_status

    class Meta:
        fields = '__all__'
        model = SystemModel


class NodeMCUDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = NodeMCUDeviceModel


class RaspiDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = RaspiDeviceModel


class SystemDetailSerializer(serializers.ModelSerializer):
    nodemcus = NodeMCUDeviceSerializer(many=True)
    raspis = RaspiDeviceSerializer(many=True)

    class Meta:
        fields = '__all__'
        model = SystemModel


class NodeMCUDetailSerializer(serializers.ModelSerializer):
    histories = serializers.SerializerMethodField()

    def get_histories(self, obj):
        histories = obj.histories.all()
        serializer = NodeMCUHistorySerializer(histories, many=True)
        return serializer.data

    class Meta:
        fields = '__all__'
        model = NodeMCUDeviceModel


class RaspiDetailSerializer(serializers.ModelSerializer):
    histories = serializers.SerializerMethodField()

    def get_histories(self, obj):
        histories = obj.histories.all()
        serializer = NodeMCUHistorySerializer(histories, many=True)
        return serializer.data

    class Meta:
        fields = '__all__'
        model = RaspiDeviceModel


class GraphSerializer(serializers.ModelSerializer):
    time = serializers.SerializerMethodField()
    temperature = serializers.SerializerMethodField()
    humidity = serializers.SerializerMethodField()

    def get_time(self, obj):
        return f'{obj.updated_at.hour}H'

    def get_temperature(self, obj):
        return float(obj.temperature)

    def get_humidity(self, obj):
        return float(obj.humidity)

    class Meta:
        fields = ('temperature', 'humidity', 'time')
        model = RaspiHistoryModel


class RaspiHistorySerializer(serializers.ModelSerializer):
    time = serializers.SerializerMethodField()
    info = serializers.SerializerMethodField()

    def get_time(self, obj):
        return format_time(obj.created_at)

    def get_info(self, obj):
        if obj.cry_status:
            return f'Baby cried detected by {obj.device.name}'
        elif obj.temperature > obj.device.system.highest_temperature:
            return f'High temperature detected by {obj.device.name}: {obj.temperature}°C'
        elif obj.temperature < obj.device.system.lowest_temperature:
            return f'Low temperature detected by {obj.device.name}: {obj.temperature}°C'
        elif obj.humidity > obj.device.system.highest_humidity:
            return f'High humidity detected by {obj.device.name}: {obj.humidity}%'
        elif obj.humidity < obj.device.system.lowest_humidity:
            return f'Low humidity detected by {obj.device.name}: {obj.humidity}%'
        else:
            return 'Something wrong'

    class Meta:
        fields = ('time', 'info')
        model = RaspiHistoryModel


class NodeMCUHistorySerializer(serializers.ModelSerializer):
    time = serializers.SerializerMethodField()
    info = serializers.SerializerMethodField()

    def get_time(self, obj):
        return format_time(obj.created_at)

    def get_info(self, obj):
        return f'Baby goes to danger zone detected by {obj.device.name}'

    class Meta:
        fields = ('time', 'info')
        model = RaspiHistoryModel


class UpdateRaspiHistorySerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = RaspiHistoryModel


class UpdateNodeMCUHistorySerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = NodeMCUHistoryModel
