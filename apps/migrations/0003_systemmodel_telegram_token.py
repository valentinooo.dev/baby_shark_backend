# Generated by Django 4.1.1 on 2022-09-07 01:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0002_rename_nodecmudevicemodel_nodemcudevicemodel_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='systemmodel',
            name='telegram_token',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
